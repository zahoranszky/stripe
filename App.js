import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Alert } from 'react-native';
import { PaymentsStripe as Stripe } from 'expo-payments-stripe';

export default function App() {

  return (
    <View style={styles.container}>
      <Text onPress={async () => {
        try {
          await Stripe.setOptionsAsync({
            publishableKey: 'pk_test_XXXXXXXX'
          });
          Alert.alert('OK');
        } catch (error) {
          Alert.alert(JSON.stringify(error));
        }
      }}>Click on me please!</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
